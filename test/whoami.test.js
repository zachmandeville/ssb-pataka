const test = require('tape')
const { promisify: p } = require('util')
const Server = require('./test-bot')

function sleep (time) {
  console.log(`sleeping: ${time}`)

  return new Promise((resolve) => {
    setTimeout(resolve, time)
  })
}

test('first time startup', async t => {
  t.plan(2)

  // start the server
  const server = Server()
  const feedId = server.id

  // TODO do a call to graphql to check it's ready,
  // because we know that by then it will have run the Context
  await sleep(1000)

  // find all of our profiles
  const profiles = await p(server.profile.findByFeedId)(feedId)
  server.close(err => {
    if (err) console.error(err)
  })

  t.deepEquals(profiles.public.length, 1, 'has 1 public profile')
  t.deepEquals(profiles.private, [], 'has no private profile')
})
