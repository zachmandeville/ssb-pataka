const Server = require('scuttle-testbot')

module.exports = function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   recpsGuard: Boolean,
  //   keys: SecretKeys
  // }

  if (!opts.serveBlobs) opts.serveBlobs = {}
  if (!opts.serveBlobs.port) opts.serveBlobs.port = 54321

  var stack = Server // eslint-disable-line
    /* @ssb-graphql/main deps */
    .use(require('ssb-blobs'))
    .use(require('ssb-serve-blobs'))

    .use(require('ssb-invite'))
    .use(require('ssb-conn'))
    .use(require('ssb-lan'))

    /* @ssb-graphql/profile deps */
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-profile'))
    .use(require('ssb-settings'))

    .use(require('ssb-recps-guard'))
    .use(require('../'))

  const ssb = stack(opts)

  return ssb
}
