const ahauServer = require('ahau-graphql-server')
const { promisify } = require('util')

const { pataka: env } = require('ahau-env')()

const Main = require('@ssb-graphql/main')
const Profile = require('@ssb-graphql/profile')
const Invite = require('@ssb-graphql/invite')
const Stats = require('@ssb-graphql/stats')
const Pataka = require('@ssb-graphql/pataka')

module.exports = async function graphqlServer (ssb, config) {
  const PORT = env.graphql.port

  const main = Main(ssb, { type: 'pataka' })
  const profile = Profile(ssb)
  const invite = Invite(ssb)
  const stats = Stats(ssb)
  const pataka = Pataka(ssb, profile.gettersWithCache)

  let context
  if (!ssb.config.graphql || ssb.config.graphql.loadContext !== false) {
    context = await promisify(main.loadContext)()
  }

  const httpServer = await ahauServer({
    schemas: [
      main,
      profile,
      invite,
      pataka,
      stats
    ],
    context,
    port: PORT,
    allowedOrigins: config.pataka && config.pataka.allowedOrigins
  }).catch(err => err)

  if (httpServer instanceof Error) throw httpServer

  ssb.close.hook((close, args) => {
    httpServer.close()
    close(...args)
  })

  return httpServer
}
